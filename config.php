<?php
define( "DEBUG", false );

define("BASE",'/Applications/MAMP/htdocs/amex/amx0004/template/');
define("ASSETS", BASE);
define("CLASSES", ASSETS.'classes/');
define("TEMPLATES", ASSETS.'html/');
define("JSON", ASSETS.'json/');
define("UPLOADS", ASSETS.'uploads/');
define("OUTPUT", BASE.'output/');
define("LITMUS", BASE.'test/');

define ('MODULES', TEMPLATES.'modules/');
define ('MODULES_MAIN', TEMPLATES.'modules/main/');
define ('MODULES_REPEATERS', TEMPLATES.'modules/repeaters/');
define ('MODULES_BLOCKS', TEMPLATES.'modules/blocks/');

define("ONEPOINTNINE_TEMPLATE", TEMPLATES.'modules/one_point_nine.html');
define("ONLINE_ACCOUNT_TEMPLATE", TEMPLATES.'modules/online-account.html');
define("MERCHANDISE_TEMPLATE", TEMPLATES.'modules/merchandise.html');

define("OMIT_XML_HEADER",true);
define("ADD_XML_HEADER",false);

// MODULES_MAIN
$main = array(
	'title' => 'title.html',
	'hero' => 'hero.html',
	'intro' => 'intro.html',
	'introleft' => 'introleft.html',
	'introright' => 'introright.html',
	'order1' => 'order1.html',
	'follower' => 'follower.html',
	'signature' => 'signature.html',
	'anyquestions' => 'anyquestions.html',
	'footer' => 'footer.html'
);

// MODULES_REPEATERS
$repeaters = [
	'terms_items' => 'terms_item.html',
	'bullettable' => 'bullettable.html',
	'bulletrow' => 'bulletrow.html',
	'numberrow' => 'numberrow.html'
];

// MODULES_BLOCKS
$blocks = array(
	"account" => "account.html",
	"didyouknow" => "didyouknow.html",
	"display" => "display.html",
	"gettingpaid" => "gettingpaid.html",
	"gettingstarted" => "gettingstarted.html",
	"merchandise" => "merchandise.html",
	"onepointnine" => "onepointnine.html",
	"onlineaccount" => "onlineaccount.html",
	"statements" => "statements.html",
	"threesteps" => "threesteps.html",
	"speedy" => "speedy.html",
	"revenue" => "revenue.html",
	"hquestions" => "hquestions.html",
	"qui" => "qui.html"
);

$emails = [
	"pop-smp" => 'title,hero,intro,order1,onepointnine,merchandise,onlineaccount,anyquestions,footer',
	"pop-non-smp" => 'title,hero,intro,order1,merchandise,onlineaccount,anyquestions,footer'
];
?>