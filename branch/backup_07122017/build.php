<?php
require 'config.php';

class AmexBuilder{
	private $jsonPath;
	private $htmlPath;
	private $outputPath;
	private $languages;
	private $templateIndex;
	private $descriptors = [];
	private $templates = [];

	private $outputIndex = [];
	private $outputCount;

	public function __construct($parameters){

		$this->jsonPath = $parameters['json'];
		$this->htmlPath = $parameters['json'];
		$this->outputPath = $parameters['output'];
		$this->templateIndex = $parameters['templates'];
		$this->languages = $parameters['languages'];
		$this->outputCount = 0;
	}

	public function setup() {

		foreach ( $this->templateIndex AS $key => $section) {
			$this->templates[$key] = $this->loadTemplateFiles($section);
		}

		// scan json directory for descriptor files
		$this->scanEmailDescriptors($this->jsonPath);

	}

	private function listEmails() {
		$currentemail = '';
		$currentLanguage = '';
		$activeEmails = [];
		$activeLanguages = [];

		foreach ( $this->descriptors AS $descriptor ) {
			if ( $descriptor['template'] == $currentemail) {
				// same template, new language?
				if ( !in_array($descriptor['language'], $activeLanguages)) {
					$activeLanguages[] = $descriptor['language'];
				}
			} else {
				// new template, reset languages
				if ( $currentemail != '' ) {
					echo "<p>{$currentemail}";
					echo ' ['.implode(',', $activeLanguages).']';
				}
				$activeLanguages = [];
				$activeLanguages[] = $descriptor['language'];
				$currentemail = $descriptor['template'];
				$activeEmails[] = $descriptor['template'];
			}
			echo "</p>\n";
		}
	}

	public function buildAllEmails() {

		foreach ( $this->descriptors AS $descriptor ) {
			// echo "<pre>".print_r($descriptor,true)."</pre>\n";
			$this->buildEmail($descriptor);
		}
		$this->outputCount = count($this->descriptors);

		// build list of output files
		$indexpage = "<p>Total emails generated: {$this->outputCount}</p>\n";

		foreach ( $this->outputIndex AS $languageGroup ) {
			$indexpage .= "<p>Language: {$languageGroup["name"]} emails: ".count($languageGroup["files"])."</p>\n";
			$indexpage .= "<ul>\n";
			foreach ( $languageGroup["files"] AS $file ) {
				$indexpage .= "<li><a href=\"{$languageGroup["code"]}/{$file}\">{$file}</a></li>\n";
			}
			$indexpage .= "</ul>\n";
		}
		// echo $indexpage;

		// save index.html
		$templateFile = file_get_contents(TEMPLATES.'modules/index.html');
		$templateFile = preg_replace ( '/\[\[filelist\]\]/', $indexpage, $templateFile);
		$fp = fopen( OUTPUT."index.html", 'w');
		fwrite( $fp, $templateFile );
		fclose( $fp );

	}

	private function buildEmail($email) {
		// echo "<p>AmexBuilder->buildEmail ".$email['output']."</p>\n";

		$this->currentLanguage = $email['language'];

		// build index of output files
		if ( !array_key_exists($this->currentLanguage, $this->outputIndex) ) {
			$this->outputIndex[$this->currentLanguage] = [];
			$this->outputIndex[$this->currentLanguage]["name"] = $this->languages[$this->currentLanguage];
			$this->outputIndex[$this->currentLanguage]["code"] = $this->currentLanguage;
			$this->outputIndex[$this->currentLanguage]["files"] = [];
		}
		$this->outputIndex[$this->currentLanguage]["files"][] = $email['output'];

		$templateFile = TEMPLATES.'modules/generic.html';

		if( file_exists( $templateFile ) ) {
			$rawTemplate = file_get_contents( $templateFile );
			$output = file_get_contents( $templateFile );

			// ---------------------------------------------------------

			// scan for links #[[]]
			preg_match_all('/\#\[\[([a-z0-9\-]+)\]\]/', $output, $links);

			// echo "<pre>".print_r($links,true)."</pre>\n";
			foreach( $links[1] AS $key => $link) {
				$output = preg_replace ( '/\#\[\['.$link.'\]\]/', $email[$link], $output);
			}

			// ---------------------------------------------------------

			preg_match_all('/\[\[([a-z0-9\-]+)\]\]/', $output, $placeholders);

			foreach( $placeholders[1] AS $key => $placeholder) {
				if ( $placeholder == 'browserview') {
					$browserViewData = $email[$placeholder];
					$browserView = $this->insertMarkedLink($browserViewData['text'],$browserViewData['links']);
					$output = preg_replace ( '/\[\['.$placeholder.'\]\]/', $browserView, $output);
				} else {
					$output = preg_replace ( '/\[\['.$placeholder.'\]\]/', $email[$placeholder], $output);
				}
			}

			// ---------------------------------------------------------

			preg_match_all('/\[\[module\:([a-z0-9\-]+)\]\]/', $output, $placeholders);

			foreach( $placeholders[1] AS $key => $module) {

				$content = '';
				$index = $this->findModuleInDescriptor($email,$module);
				if ( $index > -1 ) {
					$content = $this->buildModule($email['modules'][$index],$module);
				}

				$output = preg_replace ( '/\[\[module\:'.$module.'\]\]/', $content, $output);
			}
			// ---------------------------------------------------------

			// $output = preg_replace ( '/src\=\"img\//', 'src="../img/', $output);

			// save different languages in different directories under OUTPUT
			$this->saveEmail( OUTPUT,$email['language'],$email['output'], $output );

			$output = preg_replace ( '/img\//', 'http://production.stackworks.com/amex/amx0004/'.$email['language'].'/img/', $output);
			$this->saveEmail( LITMUS, $email['language'],$email['output'], $output );
		}
	}

	private function findModuleInDescriptor($email,$module) {
		$modules = $email['modules'];
		$moduleFound = -1;
		foreach ($modules AS $index => $m) {
			if ($module == $m['type']) {
				$moduleFound = $index;
			}
		}

		return $moduleFound;
	}

	private function findSubModuleInDescriptor($submodules,$submodule) {
		$submoduleFound = -1;

		foreach ($submodules AS $index => $m) {
			if ($submodule == $m['type']) {
				$submoduleFound = $index;
			}
		}

		return $submoduleFound;
	}

	/* ------------------------------------------------------------------------------------------------------------------------------ */
	/* ------------------------------------------------------------------------------------------------------------------------------ */

	private function buildModule($moduleData,$module) {
		$moduleType = $moduleData['type'];
		// echo "<p>AmexBuilder->buildModule ".$moduleData['type'].",".$module."</p>\n";

		if ( $moduleType == 'submodules') {
			$content = $this->buildModule_submodules($moduleData);
		} else {
			$template = $this->templates['main'][$module];
			switch ( $moduleType ) {
				case "title" : {} break;
				case "hero" : {} break;
				case "intro" : {} break;
				case "order1" : {} break;
				case "signature" : {} break;
				case "anyquestions" : {} break;
				case "footer" : {} break;
			}

			$moduleMethod = 'buildModule_'.$moduleType;
			$content = $this->$moduleMethod($moduleData,$template);
		}
		return $content;
	}

	/* ------------------------------------------------------------------------------------------------------------------------------ */
	//main modules: title,hero,intro,order1,follower,signature,anyquestions,footer
	/* ------------------------------------------------------------------------------------------------------------------------------ */

	private function buildModule_title($data,$template) {
		$template = preg_replace ( '/\[\[text\]\]/', $data['text'], $template);
		return $template;
	}

	private function buildModule_hero($data,$template) {
		$template = preg_replace ( '/\[\[img\]\]/', $data['img'], $template);
		$template = preg_replace ( '/\[\[alt\]\]/', $data['alt'], $template);
		return $template;
	}

	private function buildModule_intro($data,$template) {
		$template = preg_replace ( '/\[\[text\]\]/', $data['text'], $template);
		$template = preg_replace ( '/\[\[bottommargin\]\]/', ($data['bottommargin'] == 'yes') ? 'padding-bottom:19px;' : '', $template);
		return $template;
	}

	private function buildModule_order1($data,$template) {

		$placeholders = ['link'];
		foreach ( $placeholders AS $placeholder ) {
			$template = preg_replace ( '/\#\[\['.$placeholder.'\]\]/', $data[$placeholder], $template);
		}

		$placeholders = ['label'];
		foreach ( $placeholders AS $placeholder ) {
			$template = preg_replace ( '/\[\['.$placeholder.'\]\]/', $data[$placeholder], $template);
		}

		return $template;
	}

	private function buildModule_follower($data,$template) {
		$template = preg_replace ( '/\[\[text\]\]/', $data['text'], $template);
		return $template;
	}

	private function buildModule_signature($data,$template) {
		$template = preg_replace ( '/\[\[text\]\]/', $data['text'], $template);
		return $template;
	}

	private function buildModule_anyquestions($data,$template) {

		$placeholders = ['link'];
		foreach ( $placeholders AS $placeholder ) {
			$template = preg_replace ( '/\#\[\['.$placeholder.'\]\]/', $data[$placeholder], $template);
		}

		$placeholders = ['header','text','label'];
		foreach ( $placeholders AS $placeholder ) {
			$template = preg_replace ( '/\[\['.$placeholder.'\]\]/', $data[$placeholder], $template);
		}
		return $template;
	}

	private function buildModule_footer($data,$template) {

		// need to add exception for 'it' as footer links do not follow standard format
		if ( $this->currentLanguage == 'it') {

			$placeholders = ['flink1-link','flink2-link'];
			foreach ( $placeholders AS $placeholder ) {
				$template = preg_replace ( '/\#\[\['.$placeholder.'\]\]/', $data[$placeholder], $template);
			}

			$placeholders = ['flink1-text','flink2-text','terms-header'];
			foreach ( $placeholders AS $placeholder ) {
				$template = preg_replace ( '/\[\['.$placeholder.'\]\]/', $data[$placeholder], $template);
			}

			$itf3linktext = $data['flink3-text'];
			$itf3linktext = preg_replace ( '/\[\[link\]\]/', "<a href=\"tel:".$data['flink3-link']."\" target=\"_blank\" style=\"color:#4d4f53;text-decoration:none;\">".$data['flink3-link']."</a>", $itf3linktext);
			$template = preg_replace ( '/\[\[flink3-text\]\]/', $itf3linktext, $template);

		} else {
			$placeholders = ['flink1-link','flink2-link'];
			foreach ( $placeholders AS $placeholder ) {
				$template = preg_replace ( '/\#\[\['.$placeholder.'\]\]/', $data[$placeholder], $template);
			}

			$placeholders = ['flink1-text','flink2-text','terms-header'];
			foreach ( $placeholders AS $placeholder ) {
				$template = preg_replace ( '/\[\['.$placeholder.'\]\]/', $data[$placeholder], $template);
			}

			$itf3linktext = "<a href=\"".$data['flink3-link']."\" target=\"_blank\" style=\"color:#4d4f53;text-decoration:none;\">".$data['flink3-text']."</a>";
			$template = preg_replace ( '/\[\[flink3-text\]\]/', $itf3linktext, $template);
		}

		$termsItems = $data['terms-items'];
		$template = preg_replace ( '/\[\[terms-items\]\]/', $this->buildTermsItem($termsItems), $template);

		return $template;
	}

	/* ------------------------------------------------------------------------------------------------------------------------------ */
	/* ------------------------------------------------------------------------------------------------------------------------------ */

	private function buildModule_submodules($submodules) {
		// echo "<p>AmexBuilder->buildModule_submodules</p>\n";

		// check for presence of 'speedy' module and set width of right hand image columns for:
		// onepointnine, speedy, mechandise, online
		$column = ($this->findSubModuleInDescriptor($submodules['items'],'speedy') > -1) ? '98' : '77';

		$allContent = '';
		foreach( $submodules['items'] AS $submodule) {
			$submoduleType = $submodule['type'];
			$template = $this->templates['blocks'][$submoduleType];

			$submoduleMethod = 'buildSubmodule_'.$submoduleType;
			$content = $this->$submoduleMethod($submodule, $template, $column);
			$allContent .= $content;
		}

		return $allContent;
	}

	/* ------------------------------------------------------------------------------------------------------------------------------ */
	/* ------------------------------------------------------------------------------------------------------------------------------ */

	private function buildSubmodule_gettingstarted($data, $template) {
		$placeholders = ['alt','header'];
		foreach ( $placeholders AS $placeholder ) {
			$template = preg_replace ( '/\[\['.$placeholder.'\]\]/', $data[$placeholder], $template);
		}

 		// copy - insert links
 		// $data['copy']
 		// $data['links'] [{'text','link','colour'}]
 		$copy = $this->insertMarkedLink($data['copy'], $data['links']);
		$template = preg_replace ( '/\[\[copy\]\]/', $copy, $template);

		return $template;
	}

	private function buildSubmodule_threesteps($data, $template) {
		$placeholders = ['alt','header'];
		foreach ( $placeholders AS $placeholder ) {
			$template = preg_replace ( '/\[\['.$placeholder.'\]\]/', $data[$placeholder], $template);
		}
		$template = preg_replace ( '/\[\[items\]\]/', $this->buildThreeSteps($data['items']), $template);
		return $template;
	}

	private function buildSubmodule_didyouknow($data, $template) {
		$placeholders = ['alt','header','copy'];
		foreach ( $placeholders AS $placeholder ) {
			$template = preg_replace ( '/\[\['.$placeholder.'\]\]/', $data[$placeholder], $template);
		}
		return $template;
	}

	private function buildSubmodule_account($data, $template) {
		$placeholders = ['alt','header'];
		foreach ( $placeholders AS $placeholder ) {
			$template = preg_replace ( '/\[\['.$placeholder.'\]\]/', $data[$placeholder], $template);
		}

 		$copy = $this->insertMarkedLink($data['copy'], $data['links']);
		$template = preg_replace ( '/\[\[copy\]\]/', $copy, $template);

		// build bullets
		$template = preg_replace ( '/\[\[bullets\]\]/', $this->buildBulletPoints($data['bullets']), $template);

		return $template;
	}

	private function buildSubmodule_display($data, $template) {
		$placeholders = ['alt','header'];
		foreach ( $placeholders AS $placeholder ) {
			$template = preg_replace ( '/\[\['.$placeholder.'\]\]/', $data[$placeholder], $template);
		}

 		$copy = $this->insertMarkedLink($data['copy'], $data['links']);
		$template = preg_replace ( '/\[\[copy\]\]/', $copy, $template);

		return $template;
	}

	private function buildSubmodule_gettingpaid($data, $template) {
		$placeholders = ['img', 'alt', 'header'];
		foreach ( $placeholders AS $placeholder ) {
			$template = preg_replace ( '/\[\['.$placeholder.'\]\]/', $data[$placeholder], $template);
		}

		$copy = $this->insertMarkedLink($data['copy'], $data['links']);
		$template = preg_replace ( '/\[\[copy\]\]/', $copy, $template);

		return $template;
	}

	private function buildSubmodule_merchandise($data, $template, $column) {
		$placeholders = ['link'];
		foreach ( $placeholders AS $placeholder ) {
			$template = preg_replace ( '/\#\[\['.$placeholder.'\]\]/', $data[$placeholder], $template);
		}

		$placeholders = ['alt','header','copy', 'linktext'];
		foreach ( $placeholders AS $placeholder ) {
			$template = preg_replace ( '/\[\['.$placeholder.'\]\]/', $data[$placeholder], $template);
		}

		$template = preg_replace ( '/\[\[column\]\]/', $column, $template);

		return $template;
	}

	private function buildSubmodule_onepointnine($data, $template, $column) {

		$placeholders = ['img','alt','header','copy'];
		foreach ( $placeholders AS $placeholder ) {
			$template = preg_replace ( '/\[\['.$placeholder.'\]\]/', $data[$placeholder], $template);
		}

		$template = preg_replace ( '/\[\[column\]\]/', $column, $template);

		return $template;
	}

	private function buildSubmodule_onlineaccount($data, $template, $column) {
		$placeholders = ['link'];
		foreach ( $placeholders AS $placeholder ) {
			$template = preg_replace ( '/\#\[\['.$placeholder.'\]\]/', $data[$placeholder], $template);
		}

		$placeholders = ['alt','header','copy', 'linktext'];
		foreach ( $placeholders AS $placeholder ) {
			$template = preg_replace ( '/\[\['.$placeholder.'\]\]/', $data[$placeholder], $template);
		}

		$template = preg_replace ( '/\[\[column\]\]/', $column, $template);

		return $template;
	}

	private function buildSubmodule_statements($data, $template) {
		$placeholders = ['alt','header'];
		foreach ( $placeholders AS $placeholder ) {
			$template = preg_replace ( '/\[\['.$placeholder.'\]\]/', $data[$placeholder], $template);
		}

 		$copy = $this->insertMarkedLink($data['copy'], $data['links']);
		$template = preg_replace ( '/\[\[copy\]\]/', $copy, $template);

		return $template;
	}

	private function buildSubmodule_speedy($data, $template, $column) {
		$placeholders = ['img','alt','header','copy'];
		foreach ( $placeholders AS $placeholder ) {
			$template = preg_replace ( '/\[\['.$placeholder.'\]\]/', $data[$placeholder], $template);
		}

		$template = preg_replace ( '/\[\[column\]\]/', $column, $template);

		return $template;
	}

	private function buildSubmodule_revenue($data, $template) {

		$placeholders = ['alt','header','copy'];
		foreach ( $placeholders AS $placeholder ) {
			$template = preg_replace ( '/\[\['.$placeholder.'\]\]/', $data[$placeholder], $template);
		}
		return $template;

	}

	private function buildSubmodule_hquestions($data, $template) {
		$placeholders = ['alt','header'];
		foreach ( $placeholders AS $placeholder ) {
			$template = preg_replace ( '/\[\['.$placeholder.'\]\]/', $data[$placeholder], $template);
		}

 		$copy = $this->insertMarkedLink($data['copy'], $data['links']);
		$template = preg_replace ( '/\[\[copy\]\]/', $copy, $template);

		return $template;
	}

	/* ------------------------------------------------------------------------------------------------------------------------------ */
	/* ------------------------------------------------------------------------------------------------------------------------------ */

	private function insertLink($copy, $linktext, $linkColour = '4d4f53', $link) {
		$link = "<a href=\"$link\" target=\"_blank\" style=\"color:#$linkColour;text-decoration:underline;\">".$linktext."</a>";
		$copy = preg_replace ( '/'.$linktext.'/', $link, $copy);
		return $copy;
	}

	private function insertMarkedLink($copy, $links){
		preg_match_all('/\[\[link\]\]/', $copy, $matches, PREG_OFFSET_CAPTURE);

		for ($n = count($matches[0]) - 1; $n > -1; $n--) {
			$match = $matches[0][$n];
			$thisLinkData = $links[$n];
			$link = "<a href=\"{$thisLinkData['link']}\" target=\"_blank\" style=\"color:#{$thisLinkData['colour']};text-decoration:underline;\">{$thisLinkData['linktext']}</a>";
			$copy = substr_replace($copy, $link, $match[1], 8);
		}

		return $copy;
	}

	private function buildTermsItem($termsData) {

		$termsItemTemplate = $this->templates['repeaters']['terms_items'];
		$termsHTML = '';

		foreach( $termsData AS $termBlock ) {
			$termsItemTemplateWork = $termsItemTemplate;
			$termsItemTemplateWork = preg_replace ( '/\[\[marker\]\]/', $termBlock['marker'], $termsItemTemplateWork);
			$termsItemTemplateWork = preg_replace ( '/\[\[copy\]\]/', $termBlock['copy'], $termsItemTemplateWork);
			$termsHTML = $termsHTML . $termsItemTemplateWork;
		}
		return $termsHTML;
	}

	private function buildBulletPoints($bulletData) {

		$bulletTableTemplate = $this->templates['repeaters']['bullettable'];
		$bulletRowTemplate = $this->templates['repeaters']['bulletrow'];

		$bulletTableWorking = $bulletTableTemplate;
		$bulletContainer = '';
		foreach ( $bulletData AS $bulletpoint ) {
			$bulletRowWorking = preg_replace ( '/\[\[text\]\]/', $bulletpoint, $bulletRowTemplate);
			$bulletContainer .= $bulletRowWorking;
		}
		$bulletTableWorking = preg_replace ( '/\[\[rows\]\]/', $bulletContainer, $bulletTableWorking);

		return $bulletTableWorking;
	}

	private function buildThreeSteps($bulletData) {

		$bulletTableTemplate = $this->templates['repeaters']['bullettable'];
		$numberedRowTemplate = $this->templates['repeaters']['numberrow'];

		$bulletTableWorking = $bulletTableTemplate;
		$bulletContainer = '';
		foreach ( $bulletData AS $bulletpoint ) {
			$number = $bulletpoint['number'];
			$text = $bulletpoint['text'];
			$bulletRowWorking = preg_replace ( '/\[\[number\]\]/', $number, $numberedRowTemplate);

			if ( count($bulletpoint['links']) > 0 ) {
				$itemText = $this->insertMarkedLink($text, $bulletpoint['links']);
				$bulletRowWorking = preg_replace ( '/\[\[text\]\]/', $itemText, $bulletRowWorking);
			} else {
				$bulletRowWorking = preg_replace ( '/\[\[text\]\]/', $text, $bulletRowWorking);
			}


			$bulletContainer .= $bulletRowWorking;
		}
		$bulletTableWorking = preg_replace ( '/\[\[rows\]\]/', $bulletContainer, $bulletTableWorking);

		return $bulletTableWorking;
	}

	/* ------------------------------------------------------------------------------------------------------------------------------ */
	/* ------------------------------------------------------------------------------------------------------------------------------ */

	private function loadTemplateFiles($section) {

		$output = array();
		$path = $section['path'];
		foreach($section['files'] AS $key => $file) {
			$workingFile = $path.$file;

			if ( file_exists($workingFile) ) {
				$output[$key] = file_get_contents($workingFile);
			} else {
				$output[$key] = '';
			}

		}
		return $output;
	}

	private function scanEmailDescriptors($path) {
		echo "<p>AmexBuilder->scanEmailDescriptors {$path}</p>\n";

		$files = scandir($path);
		foreach($files as $file) {
			if ( strstr($file,'.json') ){
				$this->descriptors[] = json_decode( file_get_contents($path.$file), true );
			}
		}
	}

	private function saveEmail($outputDirectory, $language, $file, $content) {
		// echo "<p>AmexBuilder->saveEmail $outputDirectory$language/$file</p>\n";

		if (!file_exists ( $outputDirectory.$language )) {
			mkdir($outputDirectory.$language);
		}

		$fp = fopen( $outputDirectory.$language."/".$file, 'w');
		fwrite( $fp, $content );
		fclose( $fp );
	}

};
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
  <title>AMX0004 build</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="format-detection" content="telephone=no">
  <meta name="format-detection" content="email=no">
  <meta name="format-detection" content="address=no">
  <meta name="format-detection" content="date=no">
  <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700" rel="stylesheet">
  <style type="text/css">
  body{ background-color: #000;color:#fff;font-family:arial;font-size:1em;line-height:1.2em;}
  div.main{padding:20px;box-sizing:border-box;}
  a{color:#fff;text-decoration:none;}
  a:hover{color:#fff;text-decoration:underline;}
</style>
</head>
<body>
	<div class="main">
<?php
	$thisAmexBuilder = new AmexBuilder([
		"json" => JSON,
		"html" => TEMPLATES,
		"output" => OUTPUT,
		"languages" => [
			"de" => "German",
			"en" => "English",
			"es" => "Spanish",
			"fi" => "Finnish",
			"fr" => "French",
			"it" => "italian",
			"nl" => "Dutch",
			"sv" => "Swedish"
		],
		"templates" => [
			"main" => [ "files" => $main, "path" => MODULES_MAIN ],
			"repeaters" => [ "files" => $repeaters, "path" => MODULES_REPEATERS ],
			"blocks" => [ "files" => $blocks, "path" => MODULES_BLOCKS ]
		]
	]);
	$thisAmexBuilder->setup();
	$thisAmexBuilder->buildAllEmails();

?>
</div>
</body>
</html>
