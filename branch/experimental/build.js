/*
reads json descriptor files in amx/amx0004/template/json/
https://stackoverflow.com/questions/5985748/node-js-fs-read-example
*/

/* node fs file system API https://nodejs.org/api/fs.html */
const fs = require('fs');

/* https://caolan.github.io/async/ */
const async = require('async');


const config = require('./config.json');
const descriptorPath = config.json;
const HTMLPath = config.html;

const idRegExp = /id="([a-zA-z0-9]+)"/;
const fileRegExp = /([a-zA-Z0-9]+)\.html/;

let JSONFiles = []; 
let descriptors = []; 
let templateFiles = [];
let templates = {
	"main" : [],
	"blocks" : [],
	"repeaters" : []
}
let templateMap = [
	{"key" : "", "node" : ""}
];

// read HTML templates
// main
// blocks
// repeaters
function loadTemplates(wcb) {
	console.log('loadTemplates');

	async.map(
		['main', 'blocks', 'repeaters'],
		function(context,cb) {

			let filelist = [];
			let directory = config[context];

			fs.readdir(directory, function(err,files){
				let filelist = [];
				files.map(function(file){
					if (file.indexOf('.html') !== -1) {
						filelist.push(file);
					}
				});
				cb(null, {"context" : context, "directory" : directory, "files" : filelist} );
			});
			
		},
		function(err, results) {

			// convert into flat array of teimplate files to load
			results.map(function(context) {
				context.files.map(function(file){
					templateFiles.push({"context" : context.context, "directory" : context.directory, "file" : file});
				});
			});

			// load template files
			async.map(
				templateFiles,
				function(item,cb1) {
					let fullfilepath = item.directory + '/' + item.file;

					// get size of file
					fs.stat(fullfilepath, function(err,stats){

						// read contents of file
						fs.open(fullfilepath, 'r', function(err, fd){
							var buffer = new Buffer(stats.size);

							// https://stackoverflow.com/questions/5726729/how-to-parse-json-using-node-js

							fs.readFile(fullfilepath, function(err, data){
								let html = data.toString();
								let myArray = null;

								switch ( item.context ) {
									case 'main' :
									case 'repeaters' :
									{
										myArray = fileRegExp.exec(item.file);
									} break;
									case 'blocks' : {
										myArray = idRegExp.exec(html);
									} break;
								}

								cb1(null, {"context" : item.context, "key" : myArray[1], "size" : stats.size, "html" : html});
							});
						});

						
					});

				},
				function(err, results) {

					// group templates according to group
					results.map(function(template) {
						templates[template.context][template.key] = template.html;
					});

					wcb(null,true);
				}
			);			

		}
	);
}

function loadJson(results, wcb) {
	console.log('loadJson');
	wcb(null,results);
}

/*
// read contents of json/ directory
fs.readdir(descriptorPath, function(err,files){

	// get list of actual '.json' files
	files.map(function(file){
		if (file.indexOf('.json') !== -1) {
			JSONFiles.push(file);
		}
	});

	function step1() {}
	function step2() {}

	// read fson files into variable
	// step 1 get file sizes
	async.map(
		JSONFiles,
		function(item, cb){

			fs.stat(descriptorPath + '/' + item, function(err,stats){
				cb(null, {"file" : descriptorPath + '/' + item, "size" : stats.size}); // the act of calling cb stores the value in the 'results' array irrespective of what is in cb ???????????
			} );
			
		},
		function(err, results) {
			// got the size of all the files

			// step 2 read the files into memory
			async.map(
				results,
				function(item, cb){

					fs.open(item.file, 'r', function(err, fd){
						var buffer = new Buffer(item.size);

						// https://stackoverflow.com/questions/5726729/how-to-parse-json-using-node-js

						fs.readFile(item.file, function(err, data){
							var jsonData = JSON.parse(data.toString());
							cb(null, jsonData); // drop JSON object into results array
						});
					});

				},
				function (err,results) {

					// results = array of JSON objects, 1 for each email

					let languages = [];
					let languageGroup = [];
					let lgcount = 0;
					results.map(function(email,index){

						// console.log("output '%s' = %s",  email.languagecode, email.output);
						// new language found, add to LanguageGroup
						if (!languages.includes(email.languagecode)) {
							console.log("adding language: %s", email.languagecode);
							languages.push(email.languagecode);
							languageGroup.push({"language" : email.languagecode, "files" : [], "count" : 0});
							lgcount++;
						}

						// find whoch languageGroup item to add new email to
						let lg = 0;
						let fl = -1;
						do {
							let thislg = languageGroup[lg];
							if (thislg.language === email.languagecode) {
								console.log("inserting file: '%s' into '%s'", email.output, email.languagecode);
								fl = lg;
								break;
							}
							lg++;
						} while (lg < lgcount)
						
						if (fl > -1) {
							languageGroup[fl].files.push(email.output);
							languageGroup[fl].count = languageGroup[fl].count + 1;
						}
					});
					
				}
			);
		}
	);

});
*/

// https://caolan.github.io/async/docs.html#waterfall
// use waterfall, results of step 1 are accessibke in step 2
async.waterfall(
	[ loadTemplates, loadJson],
	function(step,cb){},
	function(err,results){}
);
