
const fs = require('fs');
const async = require('async');
const XLSX = require('node-xlsx');
const Entities = require('html-entities').AllHtmlEntities; // set look up table for encode function
const entities = new Entities();
var csv = require('csv');

const mappingPath = 'mappings';

var csvFiles = [];
var jtFiles = [];

function scanMappings(callback) {
	// read contents of json/ directory
	console.log("reading: %s", mappingPath);

	fs.readdir(mappingPath, function(err,files){
		if (err) {
			callback(true, 'directory doesn\'t exist');
			return;
		} else {
			// get list of actual '.json' files
			files.map(function(file){
				if (file.indexOf('.jt') !== -1) {
					jtFiles.push(file);
				}

				if (file.indexOf('.csv') !== -1) {
					csvFiles.push(file);
				}

			});

			if (jtFiles.length == 0 || csvFiles.length == 0) {
				callback(true, 'missing jt/csv files');
				return;
			}

			callback(null,'ok');
		}
	});

}

function loadFiles(results, callback) {
	console.log('loadFiles');

	async.parallel(
		[
			async.apply(loadJTfFiles,jtFiles),
			async.apply(loadCSVFiles,csvFiles)
		],
		function(err, results) {
			if (err) {
				callback(true,'files not loaded');
				return;
			} else {
				callback(null,results);
			}
		}
	);
}

function loadJTfFiles(filelist,callback) {
	console.log('loadJTfFiles');

	async.map(
		filelist,
		function(item, cb){
			async.waterfall([
				async.apply(loadJTFile,item)
			],function(err,result){
				cb(null,result);
			});

		},
		function(err, results) {
			callback(null, results);
		}
	);
}

function loadCSVFiles(filelist,callback) {
	console.log('loadCSVFiles');

	async.map(
		filelist,
		function(item, cb){
			async.waterfall([
				async.apply(loadCSVFile,item)
			],function(err,result){
				cb(null,result);
			});

		},
		function(err, results) {
			callback(null, results);
		}
	);
}

function getfileSize(file,cb) {
	fs.stat(mappingPath + '/' + file, function(err, stats){
		cb(null, stats.size );
	} );
}

function loadJTFile(file,cb) {
	// load file and parse as JSON

	fs.open(file, 'r', function(err, fd){

		// https://stackoverflow.com/questions/5726729/how-to-parse-json-using-node-js
		fs.readFile(mappingPath + '/' + file, function(err, data){
			var jsonData = JSON.parse(data.toString());
			cb(null, {"file" : mappingPath + '/' + file, "data" : jsonData['CONTENT ID'] }); // drop JSON object into results array
		});
	});

}

function loadCSVFile(file,cb) {
	// load file and parse as CSV

	fs.readFile(mappingPath + '/' + file, function(err, data){
		// var csvData = entities.encodeNonUTF(data.toString());

		csv.parse(data, function(err, data){
			cb(null, {"file" : file, "data" : data });
		});
	});
}

/*
waterfall:
	scanMappings
	loadFiles
		parrallel:
			loadJTfFiles
			loadCSVFiles
*/

async.waterfall(
	[
		scanMappings,
		loadFiles
	],
	function(error,result){
		if (!error) {
			console.log('all files loaded');

			result[1].map(function(cvsFile) {
				console.log("%s '%s':'%s'",cvsFile['file'], cvsFile['data'][0][0], cvsFile['data'][0][1]);	
			});
			
		} else {

		}

	}
);

