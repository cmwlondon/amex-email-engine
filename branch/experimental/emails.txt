var emails = [
	{
		"template" : "at-risk-smp",
		"languages" : ['de','en','es','fr','it'],
		"common" : ['browserview','merchant','title','hero','intro','questions','flinks','terms']
		"specific" : ['revenue','display','didyouknow','havequestion']
	},

	{
		"template" : "at-risk-non-smp",
		"languages" : ['de','en','es','fr','it'],
		"common" : ['browserview','merchant','title','hero','intro','questions','flinks','terms']
		"specific" : ['revenue','display','didyouknow','havequestion']
	}
];





