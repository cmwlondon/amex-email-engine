// xlsx
// https://www.npmjs.com/package/xlsx

var fs = require('fs');
var async = require('async');
var XLSX = require('node-xlsx');

var xlsxPath = 'xlsx';
var spreadsheetFiles = [];

// read contents of json/ directory
fs.readdir(xlsxPath, function(err, files){
	files.map(function(file){
		if (file.indexOf('.xlsx') !== -1) {
			spreadsheetFiles.push(file);
		}
	});

	async.map(
		spreadsheetFiles,
		function(item, cb){
			var xlsxFile = xlsxPath + '/' + item;
			fs.stat(xlsxFile, function(err,stats){
				cb(null, {"file" : xlsxPath + '/' + item, "size" : stats.size}); // the act of calling cb stores the value in the 'results' array irrespective of what is in cb ???????????
			} );
			
		},
		function(err, results) {
			console.log(results);
			/* */
			fs.open(results[0].file, 'r', function(err, fd){
				fs.readFile(fd, function(err, data){

					var sheets = XLSX.parse(data);
					var sheetNames = []; // 'EMAIL CONTENT ID EXAMPLE','UK MASTER','FR','IT','ES','DE'

					// '','UK MASTER','FR','IT','ES','DE'
					sheets.map(function(sheet,index){
						sheetNames.push(sheet.name);

						console.log("name %s rows %s", sheet.name, sheet.data.length);
						if ( index == 1 ) {
							sheet.data.map(function(row,index){
								console.log(row[0]); // row[0] = label, row[1] = data
							});
						}
					});

					console.log(sheetNames.join(','));
					
				});
			});
			/* */
		}
	);

});

/*
sheets[1]
UK MASTER: row[0]
	CONTENT ID
	SUBJECT_LINE
	BROWSER_VERSION
	AMEX_LOGO_ALT
	MERCHANT_NUMBER_LABEL
	HEADLINE
	HERO_IMAGE_ALT
	INTRO_COPY_DEAR
	INTRO_COPY
	INTRO_COPY_SIGNATURE_ALT
	INTRO_COPY_SIGNATURE
	INTRO_COPY_JOB_TITLE_1
	INTRO_COPY_JOB_TITLE_2
	PANEL_2_IMAGE_ALT
	PANEL_2_HEADLINE
	PANEL_2_COPY
	PANEL_2_CTA
	PANEL_2_CTA_LINK
	PANEL_3_IMAGE_ALT
	PANEL_3_HEADLINE
	PANEL_3_COPY
	PANEL_3_CTA
	PANEL_3_CTA_LINK
	FULL_WIDTH_PANEL_HEADLINE
	FULL_WIDTH_PANEL_COPY
	FULL_WIDTH_PANEL_CTA
	FULL_WIDTH_PANEL_CTA_LINK
	FOOTER_LINK_1_TEXT
	FOOTER_LINK_1_LINK
	FOOTER_LINK_2_TEXT
	FOOTER_LINK_2_LINK
	FOOTER_LINK_3_TEXT
	FOOTER_LINK_3_LINK
	TERMS_CONDITIONS_HEADLINE
	TERMS_CONDITIONS_COPY
	TERMS_CONDITIONS_LINK
	TERMS_CONDITIONS_FOOTNOTE_1
	TERMS_CONDITIONS_FOOTNOTE_2
	TERMS_CONDITIONS_DISCLAIMER
*/