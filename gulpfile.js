/*
npm init
sudo npm install gulp -g
sudo npm install gulp --save-dev
sudo npm install gulp-exec --save-
*/

var gulp = require('gulp');
var exec = require('child_process').exec;

// rebuild all html files in /output/if any of the JSON files in /json/ change
gulp.task('html-build', function(){
  exec('php build.php');
});

gulp.task('json-watcher', function(){
	console.log('json watcher START');
	var watcher = gulp.watch('json/*.json', ['html-build']);

	watcher.on('change', function(event) {
	  console.log('json file(s) updated');

	});
});

gulp.task('template-watcher', function(){
	console.log('template-watcher START');
	var watcher = gulp.watch('html/**/*.html', ['html-build']);

	watcher.on('change', function(event) {
	  console.log('template file(s) updated');

	});
});

gulp.task('default', ['json-watcher','template-watcher'], function(){
});